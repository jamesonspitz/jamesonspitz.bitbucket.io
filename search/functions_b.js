var searchData=
[
  ['scan_5fxyz_0',['scan_xyz',['../classpanel__driver_1_1Panel__driver.html#adbb572e1a084fb09d53e494aba4216b6',1,'panel_driver::Panel_driver']]],
  ['set_5fduty_1',['set_duty',['../classmotor__driver_1_1Motor.html#a5cd609bd1173e02326dc212e9bd44973',1,'motor_driver.Motor.set_duty()'],['../classmotordriver__l4_1_1Motor.html#ad7a077027b7f10b271ec0554b4e94664',1,'motordriver_l4.Motor.set_duty()']]],
  ['set_5fkp_2',['set_Kp',['../classClosedLoop_1_1ClosedLoop.html#a4743ddbf2fb14f44f99a84a01c36f858',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fposition_3',['set_position',['../classencoder_1_1Encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder.Encoder.set_position()'],['../classencoder__driver_1_1Encoder.html#ad5a07782f08bb4ea5e5099db62e0560a',1,'encoder_driver.Encoder.set_position()']]],
  ['set_5funits_4',['set_units',['../classBNO055_1_1BNO055.html#a7551a5b702b66b121bde625ff581700d',1,'BNO055::BNO055']]]
];
