var searchData=
[
  ['datacollection_0',['datacollection',['../classtask__encoder_1_1Task__encoder.html#aa99a9e1552e8db5eb7764dbb5cefbf41',1,'task_encoder.Task_encoder.datacollection()'],['../classtask__motor__l4_1_1Task__motor.html#a3a8a19ac2994710c33128799e8c9e9e4',1,'task_motor_l4.Task_motor.datacollection()']]],
  ['delta_1',['delta',['../classencoder_1_1Encoder.html#ad017c0a5f382fe0dac6ed8920ce90635',1,'encoder::Encoder']]],
  ['delta_5frad_2',['delta_rad',['../classClosedLoop_1_1ClosedLoop.html#ad0587b98c2d79884068f698d7e6699cd',1,'ClosedLoop.ClosedLoop.delta_rad()'],['../classtask__encoder_1_1Task__encoder.html#aba78ed1165ea8b77112393de3f99996a',1,'task_encoder.Task_encoder.delta_rad()'],['../classtask__motor__l4_1_1Task__motor.html#aeb33a933a8138113467866104f7c05eb',1,'task_motor_l4.Task_motor.delta_rad()']]],
  ['delta_5frecord_3',['delta_record',['../classtask__encoder_1_1Task__encoder.html#a69bd748f6d4398a23853568493a0861c',1,'task_encoder::Task_encoder']]],
  ['derivative_5fx_4',['derivative_x',['../classtask__controller_1_1Task__controller.html#a27b92686d70f209818166cc1d1dc8410',1,'task_controller::Task_controller']]],
  ['derivative_5fy_5',['derivative_y',['../classtask__controller_1_1Task__controller.html#a66eb6323dc5e8b9976c2402482c6c4a2',1,'task_controller::Task_controller']]],
  ['disable_6',['disable',['../classmotordriver__l4_1_1DRV8847.html#ac124045cbbf0413e1c27dd9a35eb1eb8',1,'motordriver_l4::DRV8847']]],
  ['drv8847_7',['DRV8847',['../classmotordriver__l4_1_1DRV8847.html',1,'motordriver_l4']]],
  ['duty_8',['duty',['../classtask__encoder_1_1Task__encoder.html#ac55542fb19b8d90b6e68856b6fed205d',1,'task_encoder.Task_encoder.duty()'],['../classtask__motor__l4_1_1Task__motor.html#a15a36d3bd868b6ace38eca7b72f62d35',1,'task_motor_l4.Task_motor.duty()'],['../classtask__user__l4_1_1Task__user.html#af8af9e183206dee865f947da4d7fd5cb',1,'task_user_l4.Task_user.duty()']]]
];
