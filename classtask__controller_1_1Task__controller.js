var classtask__controller_1_1Task__controller =
[
    [ "__init__", "classtask__controller_1_1Task__controller.html#abd1d6c62508396e4cd5a2b097758a6dd", null ],
    [ "run", "classtask__controller_1_1Task__controller.html#adb2abf5a4084992ba272e6caef4bb61c", null ],
    [ "derivative_x", "classtask__controller_1_1Task__controller.html#a27b92686d70f209818166cc1d1dc8410", null ],
    [ "derivative_y", "classtask__controller_1_1Task__controller.html#a66eb6323dc5e8b9976c2402482c6c4a2", null ],
    [ "integral_x", "classtask__controller_1_1Task__controller.html#ad1d73a83add338eb70b64dbb065cff90", null ],
    [ "integral_y", "classtask__controller_1_1Task__controller.html#ac4ae05744d01674827f20e2b16ac0289", null ],
    [ "kD", "classtask__controller_1_1Task__controller.html#aa852db3b0e0bf9bade1d7bb5608ea242", null ],
    [ "kI", "classtask__controller_1_1Task__controller.html#ae2c0d3c7a4b28db85ed2184ccf6a1418", null ],
    [ "kP", "classtask__controller_1_1Task__controller.html#af4053ab4d0592990351f6f13cc4a662b", null ],
    [ "prev_error_x", "classtask__controller_1_1Task__controller.html#a965c77bc74b86b28054e978b853f6dea", null ],
    [ "prev_error_y", "classtask__controller_1_1Task__controller.html#a0c06935e582bfb1523b665c940b9553c", null ],
    [ "pwm_x", "classtask__controller_1_1Task__controller.html#a7c343da22ebe3effd6ac555398205c8f", null ],
    [ "pwm_y", "classtask__controller_1_1Task__controller.html#a8a1bcc47e8ac25c7d1dc3d2605509e95", null ],
    [ "x_angle_pitch", "classtask__controller_1_1Task__controller.html#a1a7a9f1661c15e1623459726fec6a1fe", null ],
    [ "x_des", "classtask__controller_1_1Task__controller.html#a8ce3a02a57f10f880af2c44f323fd35c", null ],
    [ "y_angle_roll", "classtask__controller_1_1Task__controller.html#a15df58d1282e456ec62c2db01a3323d5", null ],
    [ "y_des", "classtask__controller_1_1Task__controller.html#a722c26969970afe76b5eac88cbe0d0b1", null ]
];