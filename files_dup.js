var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", "BNO055_8py" ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", "ClosedLoop_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_l2.py", "main__l2_8py.html", "main__l2_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "panel_driver.py", "panel__driver_8py.html", "panel__driver_8py" ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Task_controller", "classtask__controller_1_1Task__controller.html", "classtask__controller_1_1Task__controller" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_encoder", "classtask__encoder_1_1Task__encoder.html", "classtask__encoder_1_1Task__encoder" ]
    ] ],
    [ "Task_IMU.py", "Task__IMU_8py.html", [
      [ "Task_IMU.Task_IMU", "classTask__IMU_1_1Task__IMU.html", "classTask__IMU_1_1Task__IMU" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_motor", "classtask__motor_1_1Task__motor.html", "classtask__motor_1_1Task__motor" ]
    ] ],
    [ "task_panel.py", "task__panel_8py.html", [
      [ "task_panel.Task_panel", "classtask__panel_1_1Task__panel.html", null ]
    ] ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.Task_user", "classtask__user_1_1Task__user.html", "classtask__user_1_1Task__user" ]
    ] ]
];