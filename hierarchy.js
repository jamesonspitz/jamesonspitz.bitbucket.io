var hierarchy =
[
    [ "BNO055.BNO055", "classBNO055_1_1BNO055.html", null ],
    [ "ClosedLoop.ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", null ],
    [ "motordriver_l4.DRV8847", "classmotordriver__l4_1_1DRV8847.html", null ],
    [ "encoder.Encoder", "classencoder_1_1Encoder.html", null ],
    [ "encoder_driver.Encoder", "classencoder__driver_1_1Encoder.html", null ],
    [ "motor_driver.Motor", "classmotor__driver_1_1Motor.html", null ],
    [ "motordriver_l4.Motor", "classmotordriver__l4_1_1Motor.html", null ],
    [ "panel_driver.Panel_driver", "classpanel__driver_1_1Panel__driver.html", null ],
    [ "shares.Queue", "classshares_1_1Queue.html", null ],
    [ "shares.Share", "classshares_1_1Share.html", null ],
    [ "task_controller.Task_controller", "classtask__controller_1_1Task__controller.html", null ],
    [ "task_encoder.Task_encoder", "classtask__encoder_1_1Task__encoder.html", null ],
    [ "Task_IMU.Task_IMU", "classTask__IMU_1_1Task__IMU.html", null ],
    [ "task_motor.Task_motor", "classtask__motor_1_1Task__motor.html", null ],
    [ "task_motor_l4.Task_motor", "classtask__motor__l4_1_1Task__motor.html", null ],
    [ "task_panel.Task_panel", "classtask__panel_1_1Task__panel.html", null ],
    [ "task_user.Task_user", "classtask__user_1_1Task__user.html", null ],
    [ "task_user_l4.Task_user", "classtask__user__l4_1_1Task__user.html", null ],
    [ "NodeVisitor", null, [
      [ "doxypypy.AstWalker", "classdoxypypy_1_1AstWalker.html", null ]
    ] ]
];